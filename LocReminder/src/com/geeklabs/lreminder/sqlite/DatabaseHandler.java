package com.geeklabs.lreminder.sqlite;

import java.util.ArrayList;
import java.util.List;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "lr";
	private static final String TABLE_REMINDER_DETAILS = "reminder_details";
	private static final String ID = "id";
	private static final String TITLE = "title";
	private static final String DESCRIPTION = "description";
	private static final String DISTANCE = "distance";
	private static final String REMINDER_LAT = "reminder_latitude";
	private static final String REMINDER_LANG = "reminder_longitude";
	private Reminder reminder;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// 3rd argument to be passed is CursorFactory instance
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String REMINDER_DETAILS_TABLE = "CREATE TABLE " + TABLE_REMINDER_DETAILS + "(" + ID + " INTEGER PRIMARY KEY," + TITLE + " TEXT,"
				+ DESCRIPTION + " TEXT," + DISTANCE + " INTEGER," + REMINDER_LAT + " DOUBLE," + REMINDER_LANG + " DOUBLE" + ")";
		db.execSQL(REMINDER_DETAILS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_REMINDER_DETAILS);

		// Create tables again
		onCreate(db);
	}

	// code to add the new reminder
	public void addReminder(Reminder reminder) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(TITLE, reminder.getTitle());
		values.put(DESCRIPTION, reminder.getDesc());
		values.put(DISTANCE, reminder.getDist());
		values.put(REMINDER_LAT, reminder.getReminderLat());
		values.put(REMINDER_LANG, reminder.getReminderLang());

		// Inserting Row
		db.insert(TABLE_REMINDER_DETAILS, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// code to get the single reminder
	public Reminder getReminder(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_REMINDER_DETAILS, new String[] { ID, TITLE, DESCRIPTION, DISTANCE, REMINDER_LAT, REMINDER_LANG }, ID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null && cursor.moveToFirst())

			reminder = new Reminder(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getInt(3),
					cursor.getDouble(4), cursor.getDouble(5));

		cursor.close();
		// return reminder
		return reminder;
	}

	// code to get all reminders in a list view
	public List<Reminder> getAllReminders() {
		List<Reminder> reminderList = new ArrayList<Reminder>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_REMINDER_DETAILS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Reminder reminder = new Reminder();
				reminder.setId(Integer.parseInt((cursor.getString(0))));
				reminder.setTitle(cursor.getString(1));
				reminder.setDesc(cursor.getString(2));
				reminder.setDist(cursor.getInt(3));
				reminder.setReminderLat(cursor.getDouble(4));
				reminder.setReminderLang(cursor.getDouble(5));
				// Adding reminder to list
				reminderList.add(reminder);
			} while (cursor.moveToNext());
		}
		db.close();
		return reminderList;
	}

	// get all saved locations from table
	public ArrayList<OverlayItem> getAllSavedLocations() {
		// query to get all saved coordinates
		// String query = "SELECT " + REMINDER_LAT + "," + REMINDER_LANG +
		// " FROM " + TABLE_REMINDER_DETAILS;
		SQLiteDatabase db = this.getWritableDatabase();
		ArrayList<OverlayItem> locationList = new ArrayList<OverlayItem>();
		Cursor locationCursor = db
				.query(TABLE_REMINDER_DETAILS, new String[] { REMINDER_LAT, REMINDER_LANG }, null, null, null, null, null);
		locationCursor.moveToFirst();
		do {
			int latitude = (int) (locationCursor.getDouble(locationCursor.getColumnIndex(REMINDER_LAT)) * 1E6);
			int longitude = (int) (locationCursor.getDouble(locationCursor.getColumnIndex(REMINDER_LANG)) * 1E6);
			locationList.add(new OverlayItem(new GeoPoint(latitude, longitude), null, null));
		} while (locationCursor.moveToNext());
		return locationList;

	}

	// code to update the single reminder
	public int updateReminder(Reminder reminder) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(TITLE, reminder.getTitle());
		values.put(DESCRIPTION, reminder.getDesc());
		values.put(DISTANCE, reminder.getDist());
		values.put(REMINDER_LAT, reminder.getReminderLat());
		values.put(REMINDER_LANG, reminder.getReminderLang());
		
		int update = db.update(TABLE_REMINDER_DETAILS, values, ID + " = ?", new String[] { String.valueOf(reminder.getId()) });
		db.close();

		// updating row
		return update;
		
	}

	// Deleting single reminder
	public void deleteReminder(int reminderId) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.execSQL("DELETE FROM " + TABLE_REMINDER_DETAILS + " WHERE " + ID +
		// "= '" + reminderId + "'");
		db.delete(TABLE_REMINDER_DETAILS, ID + " = ?", new String[] { String.valueOf(reminderId) });
		db.close();
	}

	// Getting reminders Count
	public int getRemindersCount() {
		String countQuery = "SELECT  * FROM " + TABLE_REMINDER_DETAILS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		db.close();

		// return count
		return cursor.getCount();
	}

}