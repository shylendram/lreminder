package com.geeklabs.lreminder.sqlite;

import java.io.Serializable;

public class Reminder implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String title;
	private String desc;
	private int dist;
	private double reminderLat;
	private double reminderLang;

	public Reminder() {
	}

	public Reminder(int id, String title, String desc, int dist, double reminderLat, double reminderLang) {
		this.id = id;
		this.title = title;
		this.desc = desc;
		this.dist = dist;
		this.reminderLat = reminderLat;
		this.reminderLang = reminderLang;
	}

	public Reminder(String title, String desc, int dist, double reminderLat, double reminderLang) {
		this.title = title;
		this.desc = desc;
		this.dist = dist;
		this.reminderLat = reminderLat;
		this.reminderLang = reminderLang;
	}

	public double getReminderLat() {
		return reminderLat;
	}

	public void setReminderLat(double reminderLat) {
		this.reminderLat = reminderLat;
	}

	public double getReminderLang() {
		return reminderLang;
	}

	public void setReminderLang(double reminderLang) {
		this.reminderLang = reminderLang;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getDist() {
		return dist;
	}

	public void setDist(int dist) {
		this.dist = dist;
	}

}
