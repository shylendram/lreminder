package com.geeklabs.lreminder.util;
public final class BroadcastEvent {

	private BroadcastEvent() {}
	
	public static final String START_TRACK_SERVICE = "com.geeklabs.lreminder.START_TRACK_SERVICE";
}
