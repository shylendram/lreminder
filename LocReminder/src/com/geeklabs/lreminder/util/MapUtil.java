package com.geeklabs.lreminder.util;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class MapUtil {

	public static void animateMapToPositionAndZoom(GoogleMap googleMap, LatLng position, float zoomLevel) {
		Log.i("animate map to position and zoom","Camera update on location change inclucding zoom");
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, zoomLevel);
		googleMap.moveCamera(cameraUpdate);
	}

	public static void animateMapToPosition(GoogleMap googleMap, LatLng position) {
		if (position != null) {
			Log.i("animate map to position","Camera update on location change");
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(position);
			googleMap.moveCamera(cameraUpdate);
		} else {
			throw new IllegalStateException("Location not able to find");
		}
	}

	public static void animateMapToZoom(GoogleMap googleMap, float zoomLevel) {
		Log.i("animate map to zoom","Camera update on zoom level change");
		CameraUpdate cameraUpdate = CameraUpdateFactory.zoomTo(zoomLevel);
		googleMap.animateCamera(cameraUpdate);
	}

	public static void showInfoWindow(Marker marker) {
		if (marker != null) {
			Log.e("Info window","Info Window Open Issue");
			if (!marker.isVisible()) {
				marker.setVisible(true);
			}
			marker.showInfoWindow();
		}
	}

	public static void showGpsOptions(Context context) {
		Log.i("GPS Alert","Showing GPS alert when GPS is OFF");
		Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(gpsOptionsIntent);
	}

	public static void showWifiOptions(Context context) {
		Log.i("Network Wifi Alert","Showing Network alert when Wifi is OFF");
		Intent wifiOptionsIntent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
		context.startActivity(wifiOptionsIntent);
	}

	public static void setupStrictMode() {
//		StrictMode commonly used to catch accidental disk or network access on the application's main thread
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
	}
}
