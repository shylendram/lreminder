package com.geeklabs.lreminder.service.reciever;

import com.geeklabs.locationrem.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.widget.Toast;

public class ProximityIntentReceiver extends BroadcastReceiver {

	public static final String EVENT_ID_INTENT_EXTRA = "EventIDIntentExtraKey";
	private String extra;
	
	@SuppressWarnings("deprecation")
	@Override
	public void onReceive(Context ctx, Intent intent) {
		final String key = LocationManager.KEY_PROXIMITY_ENTERING;
		final Boolean entering = intent.getBooleanExtra(key, false);
		
		extra = intent.getStringExtra("EventIDIntentExtraKey");
		
		Toast.makeText(ctx, "entering" + extra, Toast.LENGTH_SHORT).show();
		

		NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0, intent, 0);

		Notification notification = createNotification();

		if (entering) {
			Toast.makeText(ctx, "entering", Toast.LENGTH_SHORT).show();
			notification.setLatestEventInfo(ctx, "Entering Proximity!", "", pendingIntent);
		} else {
			Toast.makeText(ctx, "exiting", Toast.LENGTH_SHORT).show();
			notification.setLatestEventInfo(ctx, "exiting Proximity!", "", pendingIntent);
		}

		notificationManager.notify((int) System.currentTimeMillis(), notification);
	}

	private Notification createNotification() {
		Notification notification = new Notification();

		notification.icon = R.drawable.ic_launcher;
		notification.tickerText = extra; 
		notification.when = System.currentTimeMillis();

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;

		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notification.defaults |= Notification.DEFAULT_LIGHTS;

		notification.ledARGB = Color.WHITE;
		notification.ledOnMS = 1500;
		notification.ledOffMS = 1500;

		return notification;
	}
	// make actions

}