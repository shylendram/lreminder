package com.geeklabs.lreminder.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.lreminder.service.reciever.ProximityIntentReceiver;

public class ProximityService extends Service {
	String proximitysd = "com.apps.ProximityService";
	int n = 0;
	//private BroadcastReceiver mybroadcast;
	private LocationManager locationManager;
	MyLocationListener locationListenerp;
	private static final String PREFERENCE_NAME = "MyPreferenceFileName";
	double lat;
	double lng;
	private static final String POINT_LATITUDE_KEY = "POINT_LATITUDE_KEY";
	private static final String POINT_LONGITUDE_KEY = "POINT_LONGITUDE_KEY";

	public ProximityService() {

	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return Service.START_NOT_STICKY; 
	}
	
	@Override
	public void onCreate() {
		//mybroadcast = new ProximityIntentReceiver();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		SharedPreferences prefs = this.getSharedPreferences(PREFERENCE_NAME,
				Context.MODE_PRIVATE);
		lat = prefs.getFloat(POINT_LATITUDE_KEY, 0);
		lng = prefs.getFloat(POINT_LONGITUDE_KEY, 0);

		float radius = 5f;
		long expiration = -1;
		// lat = cursor.getInt(MyDBAdapter.LATITUDE_COLUMN)/1E6;
		// lng = cursor.getInt(MyDBAdapter.LONGITUDE_COLUMN)/1E6;
		// lat = 12.923574064148527;
		// lng = 77.54726361483335;
		String what = "What";
		String how = "How";

		String proximitys = "ProximityService";

		IntentFilter filter = new IntentFilter(proximitys);
		//registerReceiver(mybroadcast, filter);

		Intent intent = new Intent(proximitys);
		intent.putExtra("alert", what);
		intent.putExtra("type", how);

		PendingIntent proximityIntent = PendingIntent.getBroadcast(this, n,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		locationManager.addProximityAlert(lat, lng, radius, expiration,
				proximityIntent);
		 sendBroadcast(new Intent(intent));

	}

	@Override
	public void onDestroy() {
		Toast.makeText(this, "Proximity Service Stopped", Toast.LENGTH_LONG)
				.show();
		try {
			//unregisterReceiver(mybroadcast);
		} catch (IllegalArgumentException e) {
			Log.d("reciever", e.toString());
		}

	}

	@Override
	public void onStart(Intent intent, int startid) {

		// lat = id.getInt("lat");

		Toast.makeText(this, "Proximity Service Started", Toast.LENGTH_LONG)
				.show();

		 IntentFilter filter = new IntentFilter(proximitysd);
		// registerReceiver(mybroadcast,filter);

	}
}