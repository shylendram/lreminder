package com.geeklabs.lreminder.activity;

import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.Toast;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.sqlite.DatabaseHandler;
import com.geeklabs.lreminder.sqlite.Reminder;
import com.geeklabs.lreminder.util.MapUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapViewActivity extends FragmentActivity implements
		LocationListener {

	protected GoogleMap googleMap;
	private double userCurrentLatitude, userCurrentLongitude;
	private LocationManager locationManager;
	private String provider;
	private static final String TAG = "DisplayGPSInfoActivity";
	final Context context = this;

	private NotificationCompat.Builder builder;
	private NotificationManager nm;
	private Criteria criteria;
	private Location location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_view);

		// to show notification
		builder = new NotificationCompat.Builder(this);
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		this.locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		// Choosing the best criteria depending on what is available.
		criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		// provider = LocationManager.GPS_PROVIDER; // We want to use the GPS

		// Initialize the location fields
		// Location location = locationManager.getLastKnownLocation(provider);

		locationManager.requestLocationUpdates(provider, 1000, 0, this);

		// Check is Wi-Fi available
		/*
		 * WifiManager wifi = (WifiManager)
		 * getSystemService(Context.WIFI_SERVICE); if (!wifi.isWifiEnabled()) {
		 * showAlertDailog(this, "Improve your location by enabling WiFi",
		 * "Enable Wi-Fi", NetworkType.WIFI); Log.i("WiFi", "WiFi is disabled");
		 * }
		 */
		// Make sure user device has GPS enable
		if (!isGPSAvailable()) {
			showAlertDailog(this, "Improve your location by enabling GPS",
					"Enable GPS", NetworkType.GPS);
			Log.i("GPS", "GPS is disabled");
		}

		// Check is network available
		checkNetworkAvailable(android.os.Process.myPid());

		// create Google map instance
		initGoogleMap();

		// To keep screen on until app is closed
		keepScreenActive();

		// listen when long press on map
		listenOnMapLongPress();

	}

	private void initGoogleMap() {
		// confirm that we have not already instantiated the gmap
		if (googleMap == null) {
			android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
			googleMap = ((SupportMapFragment) fragmentManager
					.findFragmentById(R.id.map)).getMap();
			Log.i("Google map", "Successfully Googlemap is opened");
			// Check if we were successful in obtaining the map.
			if (googleMap != null) {
				// show toast for Long tap on map
				Toast.makeText(this, "Long tap on map to write your reminder",
						Toast.LENGTH_LONG).show();
				// It shows current location marker on map
				googleMap.setMyLocationEnabled(true);
				
				
				
                location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
                
                if (location != null) {
             // add marker on map at current location
				LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
				googleMap.addMarker(new MarkerOptions().position(latLng).title("I am here"));

				// Creating a LatLng object for the current location

		        // Showing the current location in Google Map
		        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

				// to show current location on opening map
				googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                }
                /*double lat= location.getLatitude();
                double lng = location.getLongitude();
                LatLng ll = new LatLng(lat, lng);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 18));*/
			}
		}
	}

	// To keep screen on until app is closed
	private void keepScreenActive() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.v(TAG, "Resuming");
		locationManager.requestLocationUpdates(provider, 1000, 0, this);
	}

	@Override
	public void onProviderDisabled(String arg0) {

	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
	             // add marker on map at current location
					LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
					//googleMap.addMarker(new MarkerOptions().position(latLng).title("I am here"));

					// Creating a LatLng object for the current location

			        // Showing the current location in Google Map
			        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

					// to show current location on opening map
					googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

			userCurrentLatitude = location.getLatitude();
			userCurrentLongitude = location.getLongitude();
			/*Toast.makeText(
					this,
					"you are at: " + userCurrentLatitude + ","
							+ userCurrentLongitude, Toast.LENGTH_LONG).show();
*/
			DatabaseHandler databaseHandler = new DatabaseHandler(this);
			
			List<Reminder> reminders = databaseHandler.getAllReminders();

			for (Reminder reminder : reminders) {
				double reminderLang = reminder.getReminderLang();
				double reminderLat = reminder.getReminderLat();
				float dist = reminder.getDist();
				
				Location pointLocation = new Location("POINT_LOCATION");
				pointLocation.setLatitude(reminderLat);
				pointLocation.setLongitude(reminderLang); 
				float distance = location.distanceTo(pointLocation);
				
				if (distance <= dist) {
					NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
					Notification notification = createNotification();
					
					Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
					List<Address> addresses = null;
		            try {
		                /*
		                 * Return 1 address.
		                 */
		                addresses = geocoder.getFromLocation(reminderLat,
		                        reminderLang, 1);
		            } catch (Exception e) {
		            	e.printStackTrace();
		            }
					
		            String addr = getAddress(addresses);
		            
		            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, getIntent(), Intent.FLAG_ACTIVITY_NEW_TASK);
					notification.setLatestEventInfo(getApplicationContext(), "Reminder '" +reminder.getTitle()+"' at Address: '" +addr+ "' ", "", pendingIntent);
					
					notificationManager.notify((int) System.currentTimeMillis(), notification);
				}
			  }		
			}
	}

	private Notification createNotification() {
		Notification notification = new Notification();

		notification.icon = R.drawable.logo;
		notification.when = System.currentTimeMillis();

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;

		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notification.defaults |= Notification.DEFAULT_LIGHTS;

		notification.ledARGB = Color.WHITE;
		notification.ledOnMS = 1500;
		notification.ledOffMS = 1500;
		return notification;
	}
	
	private String getAddress(List<Address> addresses) {
		// If the reverse geocode returned an address
        if (addresses != null && addresses.size() > 0) {
            // Get the first address
            Address address = addresses.get(0);
            /*
             * Format the first line of address (if available),
             * city, and country name.
             */
            String addressText = String.format(
                    "%s, %s, %s",
                    // If there's a street address, add it
                    address.getMaxAddressLineIndex() > 0 ?
                            address.getAddressLine(0) : "",
                    // Locality is usually a city
                    address.getLocality(),
                    // The country of the address
                    address.getCountryName());
            // Return the text
            return addressText;
        } else {
            return "No address found";
        }
	}
	public void showAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MapViewActivity.this);

		// set title
		alertDialogBuilder.setTitle("Reminder Alert");

		// set dialog message
		alertDialogBuilder
				.setMessage(
						"You are near to your point of interest. Do you want to open it now")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked
								Intent intent = new Intent(
										MapViewActivity.this,
										NotificationReceiverActivity.class);
								startActivity(intent);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// to vibrate mobile
		Vibrator vibrator;
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(500);

		// to get sound
		final MediaPlayer mp = MediaPlayer.create(MapViewActivity.this,
				R.raw.alert);
		mp.start();

		// to show notification
		showNotification();

		// send SMS

		// to show it
		alertDialog.show();
	}

	private void showNotification() {
		Intent intent = new Intent(MapViewActivity.this,
				NotificationReceiverActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(
				MapViewActivity.this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);

		builder.setContentTitle("Reminder Alert")
				.setContentText(
						"You want to do some thing here..Click here to open it now")
				.setTicker("you have a reminder alert")
				.setSmallIcon(R.drawable.ic_launcher)
				.setWhen(System.currentTimeMillis())
				.setContentIntent(pendingIntent);
		nm.notify(1, builder.build());
	}

	// To show alerts for GPS and Network services
	@SuppressWarnings("deprecation")
	private void showAlertDailog(MapViewActivity mapActivity, String message,
			String buttonLabel, final NetworkType networkType) {
		AlertDialog builder = new AlertDialog.Builder(this).create();

		builder.setMessage(message);

		builder.setButton(buttonLabel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				if (NetworkType.GPS == networkType) {
					MapUtil.showGpsOptions(MapViewActivity.this);
					Log.i("Gps settings", "Gps settings successfully shown");
					initGoogleMap();
				} else {
					MapUtil.showWifiOptions(MapViewActivity.this);
					Log.i("Wifi settings", "Wifi settings successfully shown");
					initGoogleMap();
				}
			}
		});

		builder.setButton2("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				return;
			}
		});

		builder.show();
	}

	enum NetworkType {
		GPS, WIFI, MOBILE
	}

	private void listenOnMapLongPress() {

		googleMap.setOnMapLongClickListener(new OnMapLongClickListener() {

			public void onMapLongClick(final LatLng latLng) {
				// Animating to the touched position
				googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

				Log.i("Long click",
						"Successfully shown reminder save form when long click on map");

				double latitude = latLng.latitude;
				double longitude = latLng.longitude;

				Toast.makeText(
						getApplicationContext(),
						"Long clicked on " + "Latitute:" + latitude + ","
								+ "Longitude:" + longitude + " Successfully",
						Toast.LENGTH_LONG).show();

				Intent intent = new Intent(getApplicationContext(),ReminderSaveActivity.class);
				intent.putExtra("lat", latitude);
				intent.putExtra("lang", longitude);
				startActivity(intent);
			}
		});

	}

	private boolean checkNetworkAvailable(final int processId) {
		ConnectivityManager conMgr = getConnManager();
		boolean isMoblieDataEnabled = conMgr.getNetworkInfo(
				ConnectivityManager.TYPE_MOBILE).isConnected();
		boolean isWifiEnabled = conMgr.getNetworkInfo(
				ConnectivityManager.TYPE_WIFI).isConnected();
		if (!isMoblieDataEnabled && !isWifiEnabled && googleMap == null) {

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					this);
			// set title
			alertDialogBuilder.setTitle("Warning");
			alertDialogBuilder
					.setMessage("Check your network connection and try again");
			// set dialog message
			alertDialogBuilder.setCancelable(false).setNegativeButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							finish();
							moveTaskToBack(true);
						}
					});
			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			return false;
		}
		return true;
	}

	private ConnectivityManager getConnManager() {
		ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return conMgr;
	}

	// To check whether GPS,Wifi or Mobile data are available or not
	private boolean isGPSAvailable() {
		LocationManager locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map_view, menu);
		return true;
	}

}
