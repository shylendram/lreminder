package com.geeklabs.lreminder.activity;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.service.ProximityService;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page);
		final Context applicationContext = getApplicationContext();

		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// go to next screen
				Intent intent = new Intent(applicationContext, SelectOptionActivity.class);
				startActivity(intent);
			}
		});

		/*Intent i= new Intent(getApplicationContext(), ProximityService.class);
		 AlarmManager alarm = (AlarmManager) applicationContext.getSystemService(Context.ALARM_SERVICE);
		 
		 PendingIntent pIntent = PendingIntent.getService(applicationContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		 
		 Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10); // 10 sec
        
        // Repeat for every 60 seconds
		 alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 60*1000, pIntent);*/
	}

}
