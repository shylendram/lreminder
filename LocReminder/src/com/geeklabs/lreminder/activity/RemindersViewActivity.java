/**
 * 
 */
package com.geeklabs.lreminder.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.sqlite.DatabaseHandler;
import com.geeklabs.lreminder.sqlite.Reminder;

public class RemindersViewActivity extends Activity {

	private ListView listView;
	private ArrayList<String> rems = new ArrayList<String>();
	private DatabaseHandler databaseHandler = new DatabaseHandler(this);
	private ArrayAdapter<String> arrayAdapter;
	private String rem;
	private Map<Integer, Reminder> mapKeys = new HashMap<Integer, Reminder>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminders_view);

		listView = (ListView) findViewById(R.id.listView1);

		/*
		 * listView.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { Toast.makeText(getApplicationContext(),
		 * "Clicked on:" + position, Toast.LENGTH_SHORT).show(); } });
		 */

		arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_expandable_list_item_1, getReminders());
		// CustomAdapter customAdapter = new CustomAdapter();
		listView.setAdapter(arrayAdapter);
		listView.setBackgroundColor(Color.BLACK);

		// Register the ListView for Context menu
		registerForContextMenu(listView);

	}

	private List<String> getReminders() {
		DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
		List<Reminder> reminders = databaseHandler.getAllReminders();

		int j = 1;
		int i = 0;
		for (Reminder reminder : reminders) {
			rem = "Reminder " +j + "-> Title:" + reminder.getTitle() + " Descriptipn:" + " " + reminder.getDesc();
			rems.add(rem);
			mapKeys.put(i, reminder);
			i++;
			j++;
		}
		return rems;
	}

	/*
	 * @Override public void onBackPressed() { startActivity(new
	 * Intent(getApplicationContext(), ReminderSaveActivity.class)); }
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("Select The Action");
		System.out.println("item id :" + v.getId());
		menu.add(0, v.getId(), 0, "Edit");// groupId, itemId, order, title
		menu.add(0, v.getId(), 0, "Delete");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// stores the info of the selected item mainly ID
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
		if (item.getTitle() == "Edit") {
			Reminder reminder = mapKeys.get(position);
			editReminder(reminder.getId());
			// Toast.makeText(getApplicationContext(), "Reminder Saved",
			// Toast.LENGTH_LONG).show();
		} else if (item.getTitle() == "Delete") {
			Reminder reminder = mapKeys.get(position);
			deleteReminder(reminder.getId());
			Toast.makeText(getApplicationContext(), "Reminder Deleted", Toast.LENGTH_LONG).show();
		} else {
			return false;
		}
		return true;
	}

	private void deleteReminder(int id) {
		// delete reminder from db
		System.out.println("item id :" + id);
		databaseHandler.deleteReminder(id);
		finish();
		Intent intent = new Intent(getApplicationContext(), RemindersViewActivity.class);
		startActivity(intent);
		// arrayAdapter.notifyDataSetChanged();
		/*ArrayAdapter<String> ada =  (ArrayAdapter<String>) listView.getAdapter();
		ada.notifyDataSetChanged();
		runOnUiThread(new Runnable() {
		    public void run() {
		  //      arrayAdapter.notifyDataSetChanged();
		    	ArrayAdapter<String> ada =  (ArrayAdapter<String>) listView.getAdapter();
		    	ada.notifyDataSetChanged();
		    }
		});*/
	}

	private void editReminder(int id) {
		Reminder reminder = databaseHandler.getReminder(id);
		Intent intent = new Intent(getApplicationContext(), ReminderSaveActivity.class);
		intent.putExtra("reminder", reminder);
		finish();
		startActivity(intent);

	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);  
			if (resultCode == RESULT_OK) {
				Toast.makeText(getApplicationContext(), resultCode, Toast.LENGTH_SHORT).show();
			}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
		Intent intent = new Intent(getApplicationContext(), SelectOptionActivity.class);
		startActivity(intent);
	}

}
