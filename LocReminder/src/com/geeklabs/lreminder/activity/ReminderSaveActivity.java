package com.geeklabs.lreminder.activity;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.service.reciever.ProximityIntentReceiver;
import com.geeklabs.lreminder.sqlite.DatabaseHandler;
import com.geeklabs.lreminder.sqlite.Reminder;

public class ReminderSaveActivity extends Activity implements
		AdapterView.OnItemSelectedListener {

	private static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 1; // in
																		// Meters
	private static final long MINIMUM_TIME_BETWEEN_UPDATE = 1000; // in
																	// Milliseconds

	private static long POINT_RADIUS = 100; // in Meters
	private static final long PROX_ALERT_EXPIRATION = -1;

	private static final String POINT_LATITUDE_KEY = "POINT_LATITUDE_KEY";
	private static final String POINT_LONGITUDE_KEY = "POINT_LONGITUDE_KEY";

	private static final String PROX_ALERT_INTENT = "com.geeklabs.locationrem.ProximityAlert";

	private static final String PROXIMITY_INTENT_ACTION = new String("com.geeklabs.locationrem.action.PROXIMITY_ALERT");
	private LocationManager locationManager;

	private double reminderLat, reminderLang;
	private String title, desc, dist;
	private Spinner spinner;
	private int distance;
	DatabaseHandler db = new DatabaseHandler(this);

	private EditText titleEditText, descriptionEditText;

	private Integer[] radius = { 100, 500, 1000, 2000, 3000, 5000, 10000 };
	private Reminder serializableExtra;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminder_save_form);

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				MINIMUM_TIME_BETWEEN_UPDATE, MINIMUM_DISTANCECHANGE_FOR_UPDATE,
				new MyLocationListener());

		// get lat long when long tap from Main Activity
		Intent intent = getIntent();
		serializableExtra = (Reminder) intent.getSerializableExtra("reminder");
		
		reminderLat = intent.getDoubleExtra("lat", 0.0);
		reminderLang = intent.getDoubleExtra("lang", 0.0);
		
		titleEditText = (EditText) findViewById(R.id.titleEditText);
		descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
		spinner = (Spinner) findViewById(R.id.spinner1);

		spinner.setOnItemSelectedListener((OnItemSelectedListener) this);

		// Creating the ArrayAdapter instance having the radius range list
		ArrayAdapter<?> adapter = new ArrayAdapter<Object>(this,
				android.R.layout.simple_spinner_item, radius);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Setting the ArrayAdapter data on the Spinner
		spinner.setAdapter(adapter);
		
		if (serializableExtra != null) {
			titleEditText.setText(serializableExtra.getTitle());
			descriptionEditText.setText(serializableExtra.getDesc());
			int dist2 = serializableExtra.getDist();
			
			spinner.getAdapter();	
			ArrayAdapter myAdap = (ArrayAdapter) spinner.getAdapter();
			int spinnerPosition = myAdap.getPosition(dist2);
			spinner.setSelection(spinnerPosition);
			
		}
		

		findViewById(R.id.saveButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				title = titleEditText.getText().toString();
				desc = descriptionEditText.getText().toString();
				dist = spinner.getSelectedItem().toString();
				
				if (title != null && !title.isEmpty() && desc != null && !desc.isEmpty()) {
					try {
						distance = Integer.parseInt(dist);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}

					if (serializableExtra == null) {
					// store data in sqlite DB
					db.addReminder(new Reminder(title, desc, distance, reminderLat,
							reminderLang));
					}else {
						serializableExtra.setDesc(desc);
						serializableExtra.setDist(distance);
						serializableExtra.setTitle(title);
						db.updateReminder(serializableExtra);
					}

					// save proximity alert point
					saveProximityAlertPoint(title);
					
					// reading reminders from DB
					Log.d("Reading: ", "Reading all reminders..");
					List<Reminder> reminders = db.getAllReminders();

					for (Reminder rems : reminders) {
						String log = "Id: " + rems.getId() + " ,Title: "
								+ rems.getTitle() + " ,Description: "
								+ rems.getDesc() + ", Distance:" + rems.getDist()
								+ ", Latitude:" + rems.getReminderLat()
								+ ", Longitude:" + rems.getReminderLang();
						// Writing reminders to log
						Log.d("Reminders: ", log);
					}

					Toast.makeText(getApplicationContext(), "Reminder is saved successfully",
							Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(getApplicationContext(), RemindersViewActivity.class);
					ReminderSaveActivity.this.finish();
					startActivity(intent);
				}else {
					Toast.makeText(getApplicationContext(), "insert all fields", Toast.LENGTH_LONG).show();
				}
				} 
		});

	}

	private void saveProximityAlertPoint(String title) {
		saveCoordinatesInPreferences(reminderLat, reminderLang);
		addProximityAlert(reminderLat, reminderLang, title);
	}

	private void addProximityAlert(double latitude, double longitude, String title) {
/*
		Intent intent = new Intent(PROX_ALERT_INTENT);
		PendingIntent proximityIntent = PendingIntent.getBroadcast(this, 0,intent, 0);

		locationManager.addProximityAlert(latitude, longitude, POINT_RADIUS, // the radius of the central point of the alert region, in meters
				PROX_ALERT_EXPIRATION, // time for this proximity alert, in milliseconds, or -1 to indicate no expiration
				proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
				);

		IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
		registerReceiver(new ProximityIntentReceiver(), filter);*/

		// This intent will call the activity ProximityActivity
       /* Intent proximityIntent = new Intent("com.geeklabs.lreminder");					
		
        // Creating a pending intent which will be invoked by LocationManager when the specified region is
        // entered or exited
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, proximityIntent,Intent.FLAG_ACTIVITY_NEW_TASK);			        
        
        // Setting proximity alert 
        // The pending intent will be invoked when the device enters or exits the region 20 meters
        // away from the marked point
        // The -1 indicates that, the monitor will not be expired
        locationManager.addProximityAlert(latitude, longitude, 200, -1, pendingIntent);	
        */
		
		
		IntentFilter mIntentFilter = new IntentFilter(PROXIMITY_INTENT_ACTION);
		registerReceiver(new ProximityIntentReceiver(), mIntentFilter);
		
		LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    	
    	Intent intent = new Intent(PROXIMITY_INTENT_ACTION);
    	intent.putExtra("EventIDIntentExtraKey", title);
    	PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), new Random().nextInt(), intent, Intent.FLAG_ACTIVITY_NEW_TASK);
    	
    	locManager.addProximityAlert(latitude, longitude, 1000, -1, pendingIntent);
		
	}	

	private Location retrievelocationFromPreferences() {
		SharedPreferences prefs = this.getSharedPreferences(getClass()
				.getSimpleName(), Context.MODE_PRIVATE);
		Location location = new Location("POINT_LOCATION");
		location.setLatitude(prefs.getFloat(POINT_LATITUDE_KEY, 0));
		location.setLongitude(prefs.getFloat(POINT_LONGITUDE_KEY, 0));
		return location;
	}

	private void saveCoordinatesInPreferences(double latitude, double longitude) {
		SharedPreferences prefs = this.getSharedPreferences(getClass()
				.getSimpleName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putFloat(POINT_LATITUDE_KEY, (float) latitude);
		prefsEditor.putFloat(POINT_LONGITUDE_KEY, (float) longitude);
		prefsEditor.commit();
	}
	
	public class MyLocationListener implements LocationListener {
		public void onLocationChanged(Location location) {
			Location pointLocation = retrievelocationFromPreferences();
			float distance = location.distanceTo(pointLocation);
			Toast.makeText(ReminderSaveActivity.this,
					"Distance from Point:" + distance, Toast.LENGTH_LONG)
					.show();
		}

		public void onStatusChanged(String s, int i, Bundle b) {
		}

		public void onProviderDisabled(String s) {
		}

		public void onProviderEnabled(String s) {
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if (position == 0) {
		} else {
			Toast.makeText(getApplicationContext(),
					"Radius:" + String.valueOf(radius[position]), Toast.LENGTH_LONG).show();
			POINT_RADIUS = radius[position];
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		
	}

}